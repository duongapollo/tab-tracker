import Api from '@/services/Api'

export default {
  index () {
    // listing songs
    return Api().get('songs')
  },
  post (song) {
    // create a song
    return Api().post('songs', song)
  },
  show (songId) {
    // view a song
    return Api().get(`songs/${songId}`)
  }
}
