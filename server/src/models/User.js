const bcrypt = require('bcrypt')

async function hashPassword(user, options) {
  const SALT_FACTOR = 8

  if (!user.changed('password')) {
    return
  }

  return bcrypt
    .genSalt(SALT_FACTOR)
    .then(function(salt) {
      return bcrypt.hash(user.password, salt)
    })
    .then(function(hash) {
      user.setDataValue('password', hash)
    })
}

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      email: {
        type: DataTypes.STRING,
        unique: true
      },
      password: DataTypes.STRING
    },
    {
      hooks: {
        beforeCreate: hashPassword,
        beforeUpdate: hashPassword
      }
    }
  )

  User.prototype.comparePassword = async function(aPassword) {
    return await bcrypt.compare(aPassword, this.password)
  }

  return User
}
