module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('Song', {
    title: DataTypes.STRING,
    artist: DataTypes.STRING,
    genre: DataTypes.STRING,
    album: DataTypes.STRING,
    albumImageUrl: DataTypes.STRING,
    youtubeId: DataTypes.STRING,
    lyrics: DataTypes.STRING,
    tab: DataTypes.STRING
  })

  return User
}
